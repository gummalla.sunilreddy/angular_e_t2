import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { T1DefaultComponent } from './components/wrapper/t1-default/t1-default.component';
import { T1OnPushComponent } from './components/wrapper/t1-on-push/t1-on-push.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { T2DefaultComponent } from './components/wrapper/t2-default/t2-default.component';
import { T2OnPushComponent } from './components/wrapper/t2-on-push/t2-on-push.component';

@NgModule({
  declarations: [
    AppComponent,
    WrapperComponent,
    T1DefaultComponent,
    T1OnPushComponent,
    NavbarComponent,
    T2DefaultComponent,
    T2OnPushComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
