import { Component, OnInit } from '@angular/core';
import { RandomService } from 'src/app/services/random/random.service';

@Component({
  selector: 'wrapper-component',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
})
export class WrapperComponent implements OnInit {
  text: string = 'A';
  arr: string[] = ['🐊', '🐺', '🦈'];

  constructor(private _randomService: RandomService) {}

  ngOnInit(): void {}

  public changeText(): void {
    this.text = this.text + 'A';
  }

  public pushIntoArray(): void {
    this.arr.push('🦝');
  }

  public pushIntoArrayViaReference(): void {
    this.arr = [...this.arr, '🦄'];
  }

  public callNext(): void {
    this._randomService.obs.next(Math.random());
  }
}
