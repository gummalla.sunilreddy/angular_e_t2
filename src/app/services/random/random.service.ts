import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RandomService {
  public obs: BehaviorSubject<Number> = new BehaviorSubject<Number>(
    Math.random()
  );

  constructor() {}
}
